PasFluidSynth
=============

Pascal translation of [FluidSynth](https://www.fluidsynth.org/) headers
by Kirinn Bunnylin / MoonCore. Repository: https://gitlab.com/bunnylin/pasfluidsynth

This header port comes under the ZLib license, so it is safe to use the unit
with static linking. FluidSynth is available separately under the LGPL 2.1 license,
and is linked dynamically by this unit.

This port should match the FluidSynth 2.2.7 API. Not all functions have been translated!

Runs fine on Linux and on 64-bit Windows 10. Throws a mysterious access violation on
synth creation on 32-bit Windows 7 virtual machine...

Dependency: libfluidsynth runtime library. Attaches to it dynamically when you create
a TFluidSynth instance. Check `isLoaded` and `status` attributes to see if all OK.

### Caution

Per [this bug](https://github.com/FluidSynth/fluidsynth/issues/1110), the sequencer
in FluidSynth does not handle RPN's or event ordering correctly or at all. This means
it is not suitable for consistent playback of a MIDI event stream, so you'll need
a different way to do that - either using the simple fluid player, or writing the
events into the synth directly.

### Usage

```
{$mode objfpc}
uses pasfluidsynth;

var fluidsynth : TFluidSynth = NIL;
var player : TFluidSynth.PFluidPlayer = NIL;
var soundfontid : longint;

begin
	fluidsynth := TFluidSynth.Create();
	try
		writeln(fluidsynth.status);
		if NOT fluidsynth.isLoaded then exit;
		with fluidsynth do begin
			settings := new_fluid_settings();
			synth := new_fluid_synth(settings);
			soundfontid := fluid_synth_sfload(synth, '/sounds/soundfont.sf2', 1);
			audioDriver := new_fluid_audio_driver(settings, synth);
			player := new_fluid_player(synth);
			fluid_player_add(player, '/music/song.mid');
			fluid_player_play(player);
		end;
		// Let the user listen to the song! Maybe wait for a keypress to exit.
	finally
		fluidsynth.Destroy;
	end;
end.
```

### SIGSEGV segmentation fault or other trouble?

- Make sure you call TFluidSynth.methods() always with brackets, even if there are no parameters!
- Maybe the function you're calling is incorrectly translated here - doublecheck against fluidsynth.org!
- Maybe a function you need is not translated here - create an issue or pull request at the git repo!
